 <!DOCTYPE html>
<html ng-app="recipiesApp">
	<head>
		<meta charset="UTF-8">
		<title>Recipie database</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>

<body ng-controller="recipie">
<header>
	<div class="container">
		<h1>Recipie Database</h1>
	</div>
</header>

	<!-- MAIN CONTENT AND INJECTED VIEWS -->
	<div id="main" class="container">

		<!-- angular templating -->
		<!-- this is where content will be injected -->
		<div ng-view></div>

	</div>


</body>

	<script src="node_modules/angular/angular.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
	<script src="js/myscript.js"></script>
</html> 
