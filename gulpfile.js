/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins
var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    sass   = require('gulp-ruby-sass');

// Styles

gulp.task('styles', function () {
  return sass('scss/*.scss')
    .on('error', sass.logError)
    .pipe(gulp.dest('css'));
});


//Watch task
gulp.task('watch',function() {
  gulp.watch('scss/*.scss',['styles']);
});