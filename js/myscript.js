(function() {

    var app = angular.module('recipiesApp', ['ngRoute', 'ngMessages']);

    // configure our routes
    app.config(function($routeProvider) {
        $routeProvider

        // route for the home page
            .when('/', {
                templateUrl : 'pages/login.html',
                controller  : 'loginController'
            })

            // route for the about page
            .when('/recipies', {
                templateUrl : 'pages/recipies.html',
                controller  : 'recController'
            });

    });

    app.controller('recipie', function($scope, $http) {

        $http({ method: 'GET', url: 'recipes.json' }).success(function (data) {
            //add data to the scope
            $scope.recScope = data; // response data
            //console.log(data);
        }).
        error(function (data) {
            //display error message
        });
    });

    // create the controller and inject Angular's $scope
    app.controller('loginController', function($scope) {

        $scope.update = function(username, password) {
            window.location.href ='#recipies';
        };

    });

    app.controller('recController', function($scope) {
        $scope.message = 'This is not working properly.';
    });


})();
